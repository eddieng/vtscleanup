﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;

using NDesk.Options;

namespace VTSCleanup
{
    class Program
    {
        enum CleanupMode
        {
            Simple,
            Recursive
        };

        static string[] cleanDirectories = new string[] { "CONFIG", "LOG" };

        static string directory = null;
        static bool remove = true;
        static bool recursive = false;
        static string extensionsString = null;
        static string[] extensions = null;

        static Regex fileNamePattern = new Regex(@".*(20[0-9]{2}[0-1][0-9][0-3][0-9]).*");
        static Regex fileNamePattern2 = new Regex(@".*(20[0-9]{2}-[0-1][0-9]-[0-3][0-9]).*");

        static void Main(string[] args)
        {
            OptionSet options = new OptionSet().Add("d=|directory=", d => directory = d).Add("i|info", i => remove = false).Add("r|recursive", r => recursive = true).Add("e=|extensions=", e => extensions = e.ToUpper().Split(','));
            options.Parse(args);
            if (directory == null || extensions == null)
            {
                DisplayHelp();
                Environment.Exit(0);
            }
            List<string> candidates = new List<string>();
            foreach (string dir in cleanDirectories)
            {
                if (Directory.Exists(Path.Combine(directory, dir)))
                {
                    candidates.AddRange(CleanupSearch(Path.Combine(directory, dir), recursive ? CleanupMode.Recursive : CleanupMode.Simple));
                }
            }
            Console.Error.WriteLine(candidates.Count + " File(s) to Delete.");
            Console.Error.WriteLine(String.Join("\n", candidates.ToArray()));
            if (remove)
            {
                Console.Error.WriteLine("Cleaning ...");
                Cleanup(candidates);
                Console.Error.WriteLine("Done.");
            }
        }

        // Search
        static List<string> CleanupSearch(string directory, CleanupMode mode)
        {
            Console.Error.WriteLine("Directory: " + directory);
            List<string> result = new List<string>();
            try
            {
                string[] files = Directory.GetFiles(directory);
                foreach (string file in files)
                {
                    if (extensions.Contains(Path.GetExtension(file).ToUpper().Replace(".", "")))
                    {
                        if (File.GetLastWriteTime(file).Date < DateTime.Now.AddYears(-1).Date)
                        {
                            if (fileNamePattern.IsMatch(Path.GetFileName(file)))
                            {
                                try
                                {
                                    string datePart = fileNamePattern.Match(Path.GetFileName(file)).Groups[1].Value;
                                    DateTime parsed = DateTime.ParseExact(datePart, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    if (parsed.Date < DateTime.Now.AddYears(-1).Date)
                                    {
                                        result.Add(file);
                                    }
                                }
                                catch (ArgumentException e) { }
                                catch (FormatException e) { }
                            }
                            else if (fileNamePattern2.IsMatch(Path.GetFileName(file)))
                            {
                                try
                                {
                                    string datePart = fileNamePattern2.Match(Path.GetFileName(file)).Groups[1].Value;
                                    DateTime parsed = DateTime.ParseExact(datePart, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                                    if (parsed.Date < DateTime.Now.AddYears(-1).Date)
                                    {
                                        result.Add(file);
                                    }
                                }
                                catch (ArgumentException e) { }
                                catch (FormatException e) { }
                            }
                        }
                    }
                }
            }
            catch (UnauthorizedAccessException e) { }
            if (mode == CleanupMode.Recursive)
            {
                try
                {
                    string[] subdirectories = Directory.GetDirectories(directory);
                    foreach (string dir in subdirectories)
                    {
                        result.AddRange(CleanupSearch(dir, mode));
                    }
                }
                catch (UnauthorizedAccessException e) { }
                catch (IOException e) { }
            }
            return result;
        }

        // Perform
        static void Cleanup(List<string> files)
        {
            foreach (string file in files)
            {
                try
                {
                    File.Delete(file);
                }
                catch (UnauthorizedAccessException e)
                {
                    Console.Error.WriteLine("UnauthorizedAccessException: " + e.Message);
                }
                catch (IOException e)
                {
                    Console.Error.WriteLine("IOException: " + e.Message);
                }
            }
        }

        static void DisplayHelp()
        {
            Console.Error.WriteLine("Usage:");
            Console.Error.WriteLine("   -d, --directory        VTS Directory to Clean Up.");
            Console.Error.WriteLine("   -e, --extensions       Extensions, Separated by Comma.");
            Console.Error.WriteLine("   -i, --info             Display Files Only.");
            Console.Error.WriteLine("   -r, --recursive        Recursive.");
        }
    }
}
